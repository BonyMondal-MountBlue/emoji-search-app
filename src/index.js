const { default: EmojiPicker } = require("emoji-picker-react");
const { useState } = require("react");
const { render } = require("react-dom");
import React from "react";

const App = () => {
  const [choseEmoji, setChosenEmoji] = useState(null);
  const onEmojiClick = (event, emojiObject) => {
    setChosenEmoji(emojiObject);
  };
  return (
    <>
      <div style={{display:"flex",flexDirection:"column",alignItems:"center"}}>
        <h1>Emoji search App</h1>
        <EmojiPicker onEmojiClick={onEmojiClick}></EmojiPicker>

        {choseEmoji && <EmojiData choseEmoji={choseEmoji}></EmojiData>}
      </div>
    </>
  );
};
// Emoji data
const EmojiData = ({ choseEmoji }) => {
  return (
    <>
      <div style={{margin:"10px"}}>
        <strong>Emoji: </strong>
        {choseEmoji.emoji}
        <br />
        <strong>Name: </strong>
        {choseEmoji.names}
      </div>
    </>
  );
};
render(<App />, document.getElementById("root"));
